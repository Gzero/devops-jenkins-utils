# README #

### What is this repository for? ###
This utility was created in order to massively create jenkins jobs starting from its Declarative Pipeline.

### Configuration:

configs/auth.ini - Contains configuration for accessing the remote jenkins instance.
configs/logging.ini - General configuration for the logging.

configs/template.json

    {
      "global": {
        "GIT_SSH_LINK": "ssh://git@........git",
        "GIT_BRANCH": "*/develop",
        "JOB_DISABLED": "false",
        "JENKINS_CREDS_ID": "jenkins_to_bitbucket",
      },
      "views": [
        {
          "name": "view-name",
          "subviews": [
            {
              "name": "subview-name",
              "template": "templates/subview.xml",
              "replace": {
                "SUBVIEW_REGEX": "regex-expression-to-select-jobs"
              }
            }
          ]
        }
      ],
      "jobs": [
        {
          "name": "your-job-name-build",
          "template": "templates/job.xml",
          "replace": {
            "SCRIPT_PATH": "path/where/to/find/file.groovy"
          }
        ]
      }
    }
### How do i run this ? ###

action_type = (MASS_CREATE,MASS_UPDATE,MASS_DELETE,MASS_BACKUP)
file = file path starting from working directory (root of project) 


### Example run docker

    docker build -t devops-jenkins-utils:latest .

    # DEBUG
    docker run -it --entrypoint bash --name devops-jenkins-utils -v "$(pwd)/configs:/code/configs" -v "$(pwd)/jobs:/code/jobs" -v "$(pwd)/templates:/code/templates" devops-jenkins-utils:latest 
    
    # RUN
    docker run -it --name devops-jenkins-utils -v "$(pwd)/configs:/code/configs" -v "$(pwd)/jobs:/code/jobs" -v "$(pwd)/templates:/code/templates" devops-jenkins-utils:latest --action MASS_CREATE --auth configs/auth.ini --json jobs/example.json
