# set base image (host OS)
FROM python:3.9

# set the working directory in the container
WORKDIR /code

# copy the dependencies file to the working directory
COPY requirements.txt .

# install dependencies
RUN pip install -r requirements.txt

#COPY configs/ ./configs
#COPY jobs/ ./jobs
#COPY templates/ ./templates

# copy the content of the local src directory to the working directory
COPY src/ .

# Additional parameters can be added via docker run.
ENTRYPOINT [ "python", "./main.py"]