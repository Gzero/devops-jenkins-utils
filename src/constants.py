# General Constants
from enum import Enum

CONFIG_LOGGING = "configs/logging.ini"


class ActionType(Enum):
    MASS_CREATE = "MASS_CREATE"
    MASS_UPDATE = "MASS_UPDATE"
    MASS_DELETE = "MASS_DELETE"
    MASS_BACKUP = "MASS_BACKUP"
