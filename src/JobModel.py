class JobModel:

    def __init__(self, job_name, job_path, job_template_path, job_replace):
        super().__init__()
        self.name = job_name
        self.path = job_path
        self.template_path = job_template_path
        self.replace_vars = job_replace
        self.exist_remote = False

    def fullpath(self) -> str:
        if self.path:
            return "%s/%s" % (self.path, self.name)
        else:
            return self.name
