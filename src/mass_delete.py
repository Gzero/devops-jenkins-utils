import logging
from logging.config import fileConfig
from typing import List

from jenkinsapi.jenkins import Jenkins

from JobModel import JobModel
from constants import CONFIG_LOGGING, ActionType
from utility import count_non_existing_remote_jobs

fileConfig(CONFIG_LOGGING)
logger = logging.getLogger()


def validate(job_list: List[JobModel], jenkins: Jenkins) -> bool:
    logger.info("[VALIDATION] - Started")
    # Number of job user want to delete, but not found in remote jenkins instance.
    counter_not_existing_jobs = count_non_existing_remote_jobs(job_list, jenkins)

    for job in job_list:
        if not job.exist_remote:
            logger.error("[VALIDATION] [{}] - NOT FOUND - seems not existing on remote Jenkins !".format(job.name))
        else:
            logger.info("[VALIDATION] [{}] - Found".format(job.name))

    success: bool = True

    if (len(job_list) - counter_not_existing_jobs) == 0:
        logger.error(
            "[VALIDATION] - I Found [{}/{}] Jobs not existing in the remote instance, please review your configuration file.".format(
                counter_not_existing_jobs, len(job_list))
        )
        success = False

    logger.info("[VALIDATION] - Finished")

    return success


def execute(job_list: List[JobModel], server: Jenkins):
    logger.info("[START] - Jobs Mass Delete")
    for index, job in enumerate(job_list, start=1):

        if job.exist_remote:
            logger.info(
                "[{}] [{}/{}] [{}] Starting".format(ActionType.MASS_DELETE.value, index, len(job_list), job.name))
            server.delete_job(job.name)
            logger.info(
                "[{}] [{}/{}] [{}] Completed".format(ActionType.MASS_DELETE.value, index, len(job_list), job.name))
        else:
            logger.warning(
                "[{}] [{}/{}] [{}] Skip (Not existing)".format(ActionType.MASS_DELETE.value, index, len(job_list),
                                                               job.name))
    logger.info("[FINISHED] - Jobs Mass Delete")
