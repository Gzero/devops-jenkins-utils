import argparse
import logging
import os
import sys
from configparser import ConfigParser
from logging.config import fileConfig

from jenkinsapi.jenkins import Jenkins

from JobCollection import JobCollection
from constants import CONFIG_LOGGING, ActionType
from utility import yes_or_no
from tree import delete_tree, create_tree

# Load Logging config
fileConfig(CONFIG_LOGGING)
logger = logging.getLogger()
logger.info("[CONFIG] [{}] Logging configuration Loaded".format(CONFIG_LOGGING))

# Parse any input params
parser = argparse.ArgumentParser(description='Process inline parameter for the cli tool.')
parser.add_argument('--action', type=str, help='an integer for the accumulator',
                    choices=[e.value for e in ActionType], required=True)
parser.add_argument('--json', type=str, help='File which store templates config', required=True)
parser.add_argument('--auth', type=str, help='.ini File in which resides jenkins auth information', required=True)
args = parser.parse_args()

logger.info("[CONFIG] [{}] Input Param --action".format(args.action))
logger.info("[CONFIG] [{}] Input Param --json".format(args.json))
logger.info("[CONFIG] [{}] Input Param --auth".format(args.auth))

# Load Authentication information
if os.path.isfile(args.auth):
    logger.info("[CONFIG] [{}] File exists".format(args.auth))
else:
    logger.error("[CONFIG] [{}] FILE NOT FOUND".format(args.auth))
    logger.error("[CONFIG] Files found: {}".format(os.listdir("configs")))
    sys.exit(1)

authParser = ConfigParser()
authParser.read(args.auth)
logger.info("[CONFIG] [{}] Content read: {}".format(args.auth, authParser.sections()))
configAuth = authParser['jenkins']
logger.info("[CONFIG] [{}] Auth configuration loaded".format(args.auth))

# Authentication
# Link: https://thepracticalsysadmin.com/fix-the-jenkinsapi-no-valid-crumb-error/
# We need to create a crumb for the request first
# crumb=CrumbRequester(username=configAuth['username'], password=configAuth['password'], baseurl=configAuth['host'])
server: Jenkins = Jenkins(configAuth['host'], username=configAuth['username'], password=configAuth['password'])
logger.info("[AUTH] Succesfully authenticated as {} ({}) v{}".format(server.username, configAuth['host'],
                                                                     server.version))

jobCollection: JobCollection = JobCollection(server, args.json)
jobCollection.print_config_global()

validation = jobCollection.validate(ActionType[args.action])

if not validation:
    logger.error("[VALIDATION] - Failed, please review you configuration")
    sys.exit(1)

if args.action == ActionType.MASS_CREATE.value:

    create_tree(server, jobCollection.jobs_json['tree'])

    # if counter_existing_jobs > 0:
    #     validation_success = not yes_or_no(
    #         "[VALIDATION] We found [{}/{}] templates already existing in remote jenkins, do you want to ignore this - and continue with the job creations ? (im going left untouched the existing templates) "
    #             .format(counter_existing_jobs, len(job_list))
    #     )

    # ASK Manual confirmation to user before proceeding.
    should_proceed = yes_or_no(
        "[MANUAL] I'm ready, proceed to create %d templates ?" % (len(jobCollection.jobs)))
    if should_proceed:
        jobCollection.create()
    else:
        logger.warning("[MANUAL] - User choosed to NOT proceed.")

elif args.action == ActionType.MASS_UPDATE.value:
    logger.info("[START] - Jobs Mass Update - NOOP")
    # Update Jobs
    # server.reconfig_job('empty_copy', jenkins.RECONFIG_XML)
elif args.action == ActionType.MASS_DELETE.value:

    logger.info("[] [{}] I see you want to delete the following folders/views: {}".format(args.json,
                                                                                          jobCollection.jobs_json[
                                                                                              'tree']))
    logger.warning("Keep in mind that if you choose to delete, all the content of FOLDERS will be gone for good.")
    # ASK Manual confirmation to user before proceeding.
    should_proceed = yes_or_no("[MANUAL]Do you want to continue ?".format(len(jobCollection.jobs)))

    if should_proceed:
        delete_tree(jobCollection, jobCollection.jobs_json['tree'])

    # WARN: IF users have deleted the views, we will not find any jobs !
    # ASK Manual confirmation to user before proceeding.
    should_proceed = yes_or_no(
        "[MANUAL] I'm ready, proceed to delete {} templates ?".format(len(jobCollection.jobs)))
    if should_proceed:
        jobCollection.delete()
    else:
        logger.warning("[MANUAL] - User chose to NOT proceed.")

elif args.action == ActionType.MASS_BACKUP:
    logger.info("[START] - Jobs Mass Backup - NOOP")
else:
    logger.error("[ABORT] - NO Valid action detected: %s", args.action)
