import logging
from logging.config import fileConfig
from typing import List

from jenkinsapi.jenkins import Jenkins

from JobModel import JobModel
# Load Logging config
from constants import CONFIG_LOGGING

fileConfig(CONFIG_LOGGING)
logger = logging.getLogger()


def yes_or_no(question):
    check = str(input("%s (Y/N): " % question)).lower().strip()
    try:
        if check[0] == 'y':
            return True
        elif check[0] == 'n':
            return False
        else:
            print('Invalid Input')
            return yes_or_no(question)
    except Exception as error:
        print("Please enter valid inputs")
        print(error)
        return yes_or_no(question)


def count_existing_remote_jobs(local_job_list: List[JobModel], jenkins: Jenkins):
    # current_remote_jobs = fetch_remote_jobs(jenkins)

    # Perform a quick loop and mark as "remote existing" if the job already exist on remote instance:
    for job in local_job_list:
        if jenkins.has_job(job.fullpath()):
            job.exist_remote = True
        else:
            job.exist_remote = False

    # Count how many templates are already existing in remote jenkins instance.
    count = 0
    for job in local_job_list:
        if job.exist_remote:
            logger.debug("[VALIDATION] Job %s already exists in current jenkins !", job.name)
            count += 1
    logger.debug("====================")

    return count


def count_non_existing_remote_jobs(local_job_list: List[JobModel], jenkins: Jenkins):
    # current_remote_jobs = fetch_remote_jobs(jenkins)
    # Perform a quick loop and mark as "remote existing" if the job already exist on remote instance:
    for job in local_job_list:
        if jenkins.has_job(job.fullpath()):
            # if job.name in current_remote_jobs:
            job.exist_remote = True
        else:
            job.exist_remote = False

    # Count how many templates are already existing in remote jenkins instance.
    count = 0
    for job in local_job_list:
        if not job.exist_remote:
            count += 1

    return count


def fetch_remote_jobs(server: Jenkins) -> List[str]:
    # this is required in order to obtain a simple listof string with all the templates name curretly existing on remote jenkins.
    logger.info("[REMOTE] - Fetching Remote templates ... ")

    jobs_list = server.jobs.keys()
    logger.info("[REMOTE] - Jobs Found: {}".format(jobs_list))
    # url = "https://jenkins-dce20.pmidce.com/view/all/api/json?pretty=true"
    # payload = {}
    # auth = b64encode("{}:{}".format(server.username,server.password).encode("ascii")).decode("ascii")
    # headers = {
    #     'Content-Type': 'application/json',
    #     'Authorization': 'Basic {}'.format(auth)
    # }
    # response = requests.request("GET", url, headers=headers, data=payload)
    # response_json = json.loads(response.text)
    # response_jobs = response_json['templates']

    logger.info("[REMOTE-FINISHED] - Fetching Remote templates - Found: {}".format(len(jobs_list)))
    return jobs_list


