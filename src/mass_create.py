import logging
from copy import deepcopy
from logging.config import fileConfig
from typing import List

from jenkinsapi.jenkins import Jenkins

from JobModel import JobModel
from constants import CONFIG_LOGGING, ActionType
from utility import count_existing_remote_jobs

fileConfig(CONFIG_LOGGING)
logger = logging.getLogger()


def validate(job_list: List[JobModel], jenkins: Jenkins) -> bool:
    validation_success = True

    # How many templates are already existing ?
    counter_existing_jobs = count_existing_remote_jobs(job_list, jenkins)

    # If all the templates in the file are already existing, then stop here
    if (len(job_list) - counter_existing_jobs) == 0:
        logger.error("[VALIDATION] All templates are already existing ! Aborting.")
        validation_success = False
    else:
        logger.warning(
            "[VALIDATION] We found {} out of {} templates, to be already existing.".format(counter_existing_jobs,
                                                                                           len(job_list)))

    return validation_success


def execute(job_list: List[JobModel], server: Jenkins):
    logger.info("[{}] [START] - Jobs creation".format(ActionType.MASS_CREATE.value))

    local_jenkins = deepcopy(server)
    original_base_url = local_jenkins.base_server_url()

    for index, job in enumerate(job_list, start=1):
        if job.exist_remote:
            continue

        # Lets read the XML Template
        with open(job.template_path, 'r', encoding='utf-8') as file:
            data = file.read()

        # Replace all the global config + job ones.
        for key, value in job.replace_vars.items():
            data = data.replace(key, value)

        local_jenkins.baseurl = "%s/job/%s" % (original_base_url, job.path)
        local_jenkins.create_job(job.name, data)

        logger.info("[{}] {}. {} - Completed ".format(ActionType.MASS_CREATE.value, index, job.fullpath()))

    logger.info("[{}] [FINISHED] - Jobs creation".format(ActionType.MASS_CREATE.value))
