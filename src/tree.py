from copy import deepcopy
from typing import Union

from jenkinsapi.jenkins import Jenkins
from jenkinsapi.view import View
from jenkinsapi.views import Views

from JobCollection import JobCollection
from utility import logger


def delete_tree(jobCollection: JobCollection, tree):
    for entry in tree:
        delete_folder(jobCollection, entry['name'])


def delete_folder(jobCollection: JobCollection, folder_name: str):
    jenkins = jobCollection.jenkins
    endpoint = "{}/job/{}".format(jenkins.baseurl, folder_name)
    jenkins.delete_view_by_url(endpoint)
    jobCollection.deleteInFolder(folder_name)
    # Force library to reload jobs info.
    jenkins.jobs._data = []


def create_tree(jenkins: Jenkins, tree, parent=None):
    for entry in tree:
        if entry['type'] == "VIEW":
            create_view(jenkins, entry['name'], Views.CATEGORIZED_VIEW, entry, parent)
        elif entry['type'] == "FOLDER":
            create_folder(jenkins, entry['name'])
            if 'child' in entry:
                create_tree(jenkins, entry['child'], entry['name'])


def create_folder(jenkins: Jenkins, name: str, parent_name: str = None):
    if parent_name:
        endpoint = "{}/job/{}/createItem?name={}&mode={}".format(jenkins.baseurl, parent_name, name,
                                                                 "com.cloudbees.hudson.plugins.folder.Folder")
        check = jenkins.requester.get_url(
            "{}/job/{}/job/{}/api/json".format(jenkins.base_server_url(), parent_name, name))
    else:
        endpoint = "{}/createItem?name={}&mode={}".format(jenkins.baseurl, name,
                                                          "com.cloudbees.hudson.plugins.folder.Folder")
        check = jenkins.requester.get_url(
            "{}/job/{}/api/json".format(jenkins.base_server_url(), name))

    #  if NOT OK - means we received status_code different than 2xx, which means that the resource was not found (good in this case)
    if not check.ok:
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        response = jenkins.requester.post_and_confirm_status(
            endpoint,
            data={},
            headers=headers)
        logger.info("[FOLDER] [{}] Created".format(name))


def create_view(server: Jenkins, view_name: str, view_type: str, json: dict, parent: Union[View, str] = None) -> View:
    views = server.views.keys()

    if type(parent) is str:  # if parent is a folder.
        server = deepcopy(server)
        server.baseurl = "%s/job/%s" % (server.base_server_url(), parent)
    elif type(parent) is View:  # if parent is a view
        server = parent

    if view_type == Views.CATEGORIZED_VIEW:
        view_config = create_subview_config(json['template'], json['name'], json['replace'])
    else:
        view_config = {}

    if view_name not in views:
        logger.info("[VIEW] [{}] does not exist, creating one.".format(view_name))
        new_view = server.views.create(view_name, view_type, view_config)
        if new_view is None:
            logger.error('[VIEW] [{}] was not created'.format(view_name))
        else:
            logger.info('[VIEW] [{}] has been created'.format(new_view.name))
        return new_view
    else:
        logger.info("[VIEW] [{}] already exist.".format(view_name))
        return server.views[view_name]


def create_subview_config(template_path, name, replace: dict):
    with open(template_path, "r", encoding='utf-8') as file:
        xmlconfig = file.read()

    xmlconfig = xmlconfig.replace("SUBVIEW_NAME", name)
    for k, v in replace.items():
        xmlconfig = xmlconfig.replace(k, v)

    return xmlconfig