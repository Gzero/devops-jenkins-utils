import json
import logging
import sys
from copy import deepcopy
from logging.config import fileConfig
from typing import List

from jenkinsapi.jenkins import Jenkins

import utility
from JobModel import JobModel
from constants import ActionType, CONFIG_LOGGING

fileConfig(CONFIG_LOGGING)
logger = logging.getLogger()


class JobCollection:

    def __init__(self, jenkins: Jenkins, file_path) -> None:
        super().__init__()
        self.jenkins = jenkins
        self.jobs: List[JobModel] = []
        self.jobs_json = None
        self.parse_json(file_path)

    def parse_json(self, file_path: str):
        logger.info("[CONFIG] [{}] Reading jobs config".format(file_path))

        with open(file_path, "r", encoding='utf-8') as jsonfile:
            self.jobs_json = json.load(jsonfile)

        for job_config in self.jobs_json['jobs']:
            merged_replace = self.jobs_json['global'] | job_config['replace']  # This black magic, requires python 3.9+
            self.jobs.append(JobModel(job_config['name'], job_config['path'], job_config['template'], merged_replace))

        logger.info("[CONFIG] [{}] Succesfully read {} jobs config".format(file_path, len(self.jobs)))
        logger.info("========================")

        logger.debug("[CONFIG] Jobs config read from {}".format(file_path))
        for index, job in enumerate(self.jobs, start=1):
            logger.debug("[CONFIG] %d. Job: %s | Template: %s", index, job.name, job.template_path)
        logger.debug("========================")

    def validate(self, action_type: ActionType):
        logger.info("[VALIDATION] - Started")

        validation_success = True

        count = utility.count_existing_remote_jobs(self.jobs, self.jenkins)

        if action_type == ActionType.MASS_CREATE:
            # If all the templates in the file are already existing, then stop here
            if (len(self.jobs) - count) == 0:
                logger.error("[VALIDATION] All templates are already existing ! Aborting.")
                validation_success = False
            else:
                logger.warning(
                    "[VALIDATION] We found {} out of {} templates, to be already existing.".format( count, len(self.jobs)))
        elif action_type == ActionType.MASS_DELETE:
            if len(self.jobs) == 0:
                logger.warning("[VALIDATION] - Oops, Seems we don't have any job to delete !")
            elif(len(self.jobs) - count) == 0:
                logger.info("[VALIDATION] All jobs found on remote instance")
            else:
                logger.error("[VALIDATION] ({}) jobs were not found".format(len(self.jobs) - count))
                validation_success = False

        logger.info("[VALIDATION] - Finished")

        return validation_success

    def create(self):
        validation = self.validate(ActionType.MASS_CREATE)

        if not validation:
            logger.error("[VALIDATION] - Validation Failed, please check your configuration")
            sys.exit(1)

        logger.info("[{}] [START] - Jobs creation".format(ActionType.MASS_CREATE.value))

        # Ok i know this is bad, but don't judge me !
        # In order to be able to create a job inside a specific view, we need to trick the jenkinsapi library
        local_jenkins = deepcopy(self.jenkins)
        original_base_url = local_jenkins.base_server_url()

        for index, job in enumerate(self.jobs, start=1):
            if job.exist_remote:
                continue

            # Lets read the XML Template
            with open(job.template_path, 'r', encoding='utf-8') as file:
                data = file.read()

            # Replace all the global config + job ones.
            for key, value in job.replace_vars.items():
                data = data.replace(key, value)


            local_jenkins.baseurl = "%s/job/%s" % (original_base_url, job.path)
            local_jenkins.create_job(job.name, data)

            logger.info("[{}] {}. {} - Completed ".format(ActionType.MASS_CREATE.value, index, job.fullpath()))

        logger.info("[{}] [FINISHED] - Jobs creation".format(ActionType.MASS_CREATE.value))

    def delete(self):
        validation = self.validate(ActionType.MASS_DELETE)

        if not validation:
            logger.error("[VALIDATION] - Validation Failed, please check your configuration")
            sys.exit(1)

        logger.info("[START] - Jobs Mass Delete")

        for index, job in enumerate(self.jobs, start=1):

            if job.exist_remote:
                logger.info(
                    "[{}] [{}/{}] [{}] Starting".format(ActionType.MASS_DELETE.value, index, len(self.jobs), job.fullpath()))
                self.jenkins.delete_job(job.fullpath())
                logger.info(
                    "[{}] [{}/{}] [{}] Completed".format(ActionType.MASS_DELETE.value, index, len(self.jobs), job.fullpath()))
            else:
                logger.warning(
                    "[{}] [{}/{}] [{}] Skip (Not existing)".format(ActionType.MASS_DELETE.value, index, len(self.jobs),
                                                                   job.name))
        logger.info("[FINISHED] - Jobs Mass Delete")

    """
    Remove every jobs inside a specific folder
    NOTE: Only from local storage (not on remote jenkins)
    """
    def deleteInFolder(self,folder_name):
        self.jobs = [job for job in self.jobs if job.path != folder_name]

    def print_config_global(self):
        # Print all global configs
        logger.info("[CONFIG] Global Configuration Applied by default to all templates:")
        for k, v in self.jobs_json['global'].items():
            logger.info("[CONFIG] * %s => %s", k, v)
        logger.info("========================")

