import logging
import time
import unittest
from configparser import ConfigParser
from logging.config import fileConfig

from jenkinsapi.jenkins import Jenkins

import tree
import utility

CONFIG_AUTH = "configs/auth.ini"
CONFIG_JOBS = "configs/templates-create-td-build.json"
CONFIG_LOGGING = "configs/logging.ini"

# Load Logging config
fileConfig(CONFIG_LOGGING)
logger = logging.getLogger()

# Load Authentication information
parser = ConfigParser()
parser.read(CONFIG_AUTH)
configAuth = parser['jenkins']


class MyTestCase(unittest.TestCase):
    def test_fetch_remote_job_list_http(self):

        server: Jenkins = Jenkins(configAuth['host'], username=configAuth['username'], password=configAuth['password'])
        logger.info(
            "[AUTH] Succesfully authenticated: {} from Jenkins ({}) v{}".format(server.username, configAuth['host'],
                                                                                server.version))

        jenkins_job = server.get_jobs_list()
        for j in jenkins_job: logger.info(j)

    def test_fetch_remote_job_list_https(self):
        # Load Authentication information
        parser = ConfigParser()
        parser.read(CONFIG_AUTH)
        configAuth = parser['jenkins']

        server: Jenkins = Jenkins("https://jenkins-dce20.pmidce.com", username=configAuth['username'],
                                  password=configAuth['password'])
        logger.info(
            "[AUTH] Succesfully authenticated: {} from Jenkins ({}) v{}".format(server.username, configAuth['host'],
                                                                                server.version))

        jenkins_job = server.get_jobs_list()
        for j in jenkins_job: logger.info(j)

    def test_fetch_remote_jobs(self):
        jenkins: Jenkins = Jenkins(configAuth['host'], username=configAuth['username'], password=configAuth['password'])
        utility.fetch_remote_jobs(jenkins)

    def test_create_folder(self):
        jenkins: Jenkins = Jenkins(configAuth['host'], username=configAuth['username'], password=configAuth['password'])
        utility.create_folder_tree(jenkins)

    def test_delete_folder(self):
        jenkins: Jenkins = Jenkins(configAuth['host'], username=configAuth['username'], password=configAuth['password'])

        #old_baseurl= jenkins.baseurl
        tree.create_folder(jenkins, "test")

        with open("templates/job.xml", 'r', encoding='utf-8') as file:
            data = file.read()
        #jenkins.baseurl = "%s/job/%s" % (jenkins.baseurl, "test")
        jenk = jenkins.get_jenkins_obj_from_url("%s/job/%s" % (jenkins.baseurl, "test"))

        jenk.create_job("tests", data)

        logger.info("Job found: {}".format(jenkins.has_job("test/tests")))

        tree.delete_folder(jenkins, "test")
        # force jennkins to reload its config.


        time.sleep(5)
        logger.info("Job found: {}".format(jenkins.has_job("test/tests")))


if __name__ == '__main__':
    unittest.main()
